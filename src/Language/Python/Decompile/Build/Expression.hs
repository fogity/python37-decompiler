{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Build.Expression where

import Control.Monad
import Data.Bits

import qualified Data.HashMap.Strict as HM

import Language.Python.Decompile.AST
import Language.Python.Decompile.Build.Monad
import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode
import Language.Python.Decompile.Operation

-- | Builds instructions that do not generate statements.
buildInStack :: Instruction -> Build ()
buildInStack Instruction {..}
  | Just o <- HM.lookup opcode unaryOps
  = Unary o <$> pop >>= push
  | Just o <- HM.lookup opcode binaryOps
  = flip (Binary False o) <$> pop <*> pop >>= push
  | Just o <- HM.lookup opcode inplaceOps
  = flip (Binary True o) <$> pop <*> pop >>= push
buildInStack Instruction {..} = case opcode of
  -- Indexing.
  BINARY_SUBSCR -> flip Index <$> pop <*> pop >>= push
  -- List creation.
  BUILD_LIST -> replicateM argument pop
    >>= push . Collection List . map Item . reverse
  -- Map creation.
  BUILD_MAP -> replicateM argument (flip Mapping <$> pop <*> pop)
    >>= push . Collection Dict . reverse
  -- Map merging.
  BUILD_MAP_UNPACK_WITH_CALL -> replicateM argument pop
    >>= push . foldl mergeCollections (Collection Dict []) . reverse
  -- Map creation with constant keys.
  BUILD_CONST_KEY_MAP -> pop >>= \case
    Collection Tuple ks
      -> mapM (\ (Item e) -> Mapping e <$> pop) (reverse ks)
      >>= push . Collection Dict . reverse
    t -> error $ "BUILD_CONST_KEY_MAP: Found non-tuple " ++ show t
  -- Set creation.
  BUILD_SET -> replicateM argument pop
    >>= push . Collection Set . map Item . reverse
  -- Slice creation.
  BUILD_SLICE -> replicateM argument pop >>= push . Slice . reverse
  -- String building.
  BUILD_STRING -> replicateM argument pop >>= push . String . reverse
  -- Tuple creation.
  BUILD_TUPLE -> replicateM argument pop
    >>= push . Collection Tuple . map Item . reverse
  -- Tuple merging.
  BUILD_TUPLE_UNPACK_WITH_CALL -> replicateM argument pop
    >>= push . foldl mergeCollections (Collection Tuple []) . reverse
  -- Function call.
  CALL_FUNCTION -> replicateM argument pop >>= \ args ->
    Call <$> pop <*> return (map Item $ reverse args)  >>= push
  -- Function call with kw arguments.
  CALL_FUNCTION_KW -> pop >>= \case
    Collection Tuple ks -> replicateM argument pop >>= \ args
      -> let
      kws = zipWith (\ (Item e) e' -> Mapping e e') (reverse ks) args
      pos = map Item $ drop (length ks) args
      in Call <$> pop <*> return (reverse $ kws ++ pos) >>= push
    t -> error $ "CALL_FUNCTION_KW: Found non-tuple " ++ show t
  -- Function call with arguments contained in a tuple and possibly a map.
  CALL_FUNCTION_EX -> do
    kws <- if argument .&. 0x1 /= 0
      then pop >>= \case
      Collection _ is -> return is
      e               -> return [MappingUnpack e]
      else return []
    pos <- pop >>= \case
      Collection _ is -> return is
      e               -> return [ItemUnpack e]
    Call <$> pop <*> return (pos ++ kws) >>= push
  -- A binary comparison operation.
  COMPARE_OP -> flip (Binary False $ toCompareOp argument) <$> pop <*> pop
    >>= push
  -- Pushes a second copy of the top expression on the stack.
  DUP_TOP -> pop >>= \ e -> push e >> push e
  -- Pushes second copies of the top two expressions on the stack.
  DUP_TOP_TWO -> pop >>= \ e1 -> pop >>= \ e2
    -> replicateM_ 2 $ push e2 >> push e1
  -- Uses the expression to format a string.
  FORMAT_VALUE -> do
    when (argument .&. 0x4 /= 0) $ void pop
    pop >>= push . Format
  -- Imports and pushes a module onto the stack.
  IMPORT_NAME -> pop >> pop >> TModule <$> getName argument >>= push
  -- Imports a name from a module and pushes it onto the stack.
  IMPORT_FROM -> pop >>= \ m -> push m >> Attr m <$> getName argument >>= push
  -- Attribute access.
  LOAD_ATTR -> Attr <$> pop <*> getName argument >>= push
  -- Pushes the builtin build class function.
  LOAD_BUILD_CLASS -> push TBuildClass
  -- Constant loading.
  LOAD_CONST -> getConst argument >>= push . valueToExpr
  -- Variable loading.
  LOAD_FAST -> getVar argument >>= push . Name
  -- Free variable loading.
  LOAD_DEREF -> getCellFree argument >>= push . Name
  -- Name loading.
  LOAD_NAME -> getName argument >>= push . Name
  -- Function loading.
  MAKE_FUNCTION -> pop >> pop >>= \case
    TFunction c _ -> do
      when (argument .&. 0x8 /= 0) $ void pop
      annotations <- if argument .&. 0x4 /= 0
        then pop >>= \case
        Collection _ is -> return $ Just is
        _ -> error "MAKE_FUNCTION: expected collection for annotations"
        else return Nothing
      defaultKW <- if argument .&. 0x2 /= 0
        then pop >>= \case
        Collection _ is -> return $ Just is
        _ -> error "MAKE_FUNCTION: expected collection for defaultKW"
        else return Nothing
      defaultPos <- if argument .&. 0x1 /= 0
        then pop >>= \case
        Collection _ is -> return $ Just is
        _ -> error "MAKE_FUNCTION: expected collection for defaultPos"
        else return Nothing
      push $ TFunction c ParamData {..}
    c -> error $ "MAKE_FUNCTION: Found non-code " ++ show c
  -- Moves the top expression down two places in the stack.
  ROT_THREE -> pop >>= \ e1 -> pop >>= \ e2 -> pop >>= \ e3
    -> push e1 >> push e3 >> push e2
  -- Moves the top expression down one places in the stack.
  ROT_TWO -> pop >>= \ e1 -> pop >>= \ e2 -> push e1 >> push e2
  -- Yield a value.
  YIELD_VALUE -> Yield <$> pop >>= push
  -- Yield a value from a generator.
  YIELD_FROM -> pop >> YieldFrom <$> pop >>= push
  _ -> error $ "buildInStack: Unsupported opcode " ++ show opcode

-- | Mappings for unary operation opcodes.
unaryOps :: HM.HashMap OpCode UnOp
unaryOps = HM.fromList
  [ (UNARY_INVERT, Invert)
  , (UNARY_NEGATIVE, Negative)
  , (UNARY_NOT, Not)
  , (UNARY_POSITIVE, Positive)
  ]

-- | Mappings for binary operation opcodes.
binaryOps :: HM.HashMap OpCode BinOp
binaryOps = HM.fromList
  [ (BINARY_ADD, Add)
  , (BINARY_AND, And)
  , (BINARY_FLOOR_DIVIDE, FloorDivide)
  , (BINARY_LSHIFT, LeftShift)
  , (BINARY_MATRIX_MULTIPLY, MatrixMultiply)
  , (BINARY_MODULO, Modulo)
  , (BINARY_MULTIPLY, Multiply)
  , (BINARY_OR, Or)
  , (BINARY_POWER, Power)
  , (BINARY_RSHIFT, RightShift)
  , (BINARY_SUBTRACT, Subtract)
  , (BINARY_TRUE_DIVIDE, Divide)
  , (BINARY_XOR, XOr)
  ]

-- | Mappings for in-place binary operation opcodes.
inplaceOps :: HM.HashMap OpCode BinOp
inplaceOps = HM.fromList
  [ (INPLACE_ADD, Add)
  , (INPLACE_AND, And)
  , (INPLACE_FLOOR_DIVIDE, FloorDivide)
  , (INPLACE_LSHIFT, LeftShift)
  , (INPLACE_MATRIX_MULTIPLY, MatrixMultiply)
  , (INPLACE_MODULO, Modulo)
  , (INPLACE_MULTIPLY, Multiply)
  , (INPLACE_OR, Or)
  , (INPLACE_POWER, Power)
  , (INPLACE_RSHIFT, RightShift)
  , (INPLACE_SUBTRACT, Subtract)
  , (INPLACE_TRUE_DIVIDE, Divide)
  , (INPLACE_XOR, XOr)
  ]

-- | Builds an expression from a value.
valueToExpr :: Value -> Expr
valueToExpr = \case
  VCode c -> TFunction c $ ParamData Nothing Nothing Nothing
  VNone -> None
  VEllipsis -> Ellipsis
  VBasic t s -> Value t s
  VTuple vs -> Collection Tuple $ map (Item . valueToExpr) vs
  VFrozenSet vs -> Collection Set $ map (Item . valueToExpr) vs

-- | Combines expressions into a collection expression.
mergeCollections :: Expr -> Expr -> Expr
mergeCollections (Collection c is) (Collection c' is')
  | c == c'   = Collection c $ is ++ is'
  | otherwise = error
    $ "mergeCollections: Trying to merge a " ++ show c ++ " and a " ++ show c'
mergeCollections e (Collection c is)
  = mergeCollections (toSingleton c e) (Collection c is)
mergeCollections (Collection c is) e
  = mergeCollections (Collection c is) (toSingleton c e)
mergeCollections a b = error
  $ "mergeCollections: Cannot merge " ++ show a ++ " and " ++ show b

-- | Wraps an expression in a collection expression.
toSingleton :: Collection -> Expr -> Expr
toSingleton Dict e = Collection Dict [MappingUnpack e]
toSingleton c    e = Collection c    [ItemUnpack e]
