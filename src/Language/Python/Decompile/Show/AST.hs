{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Show.AST where

import Data.Bits
import Data.List
import qualified Data.Text as T

import Language.Python.Decompile.AST
import Language.Python.Decompile.Code
import Language.Python.Decompile.Operation
import Language.Python.Decompile.Show
import Language.Python.Decompile.Show.Operation

import Language.Python.Decompile.Build.AST

-- | Shows an AST.
-- First argument is the indentation level.
showAST :: Int -> AST -> T.Text
showAST i = interMap "\n" $ showStatement i False

-- | Shows a statement.
-- First argument is the indentation level.
-- Second argument specifies whether the current statement is decorated.
showStatement :: Int -> Bool -> Statement -> T.Text
showStatement i d = \case
  Decorator e s -> (if d then "" else "\n")
    <> indent i "@" <> showExpr e <> "\n" <> showStatement i True s
  Def e c p a -> (if d then "" else "\n")
    <> indent i "def " <> showExpr e <> inside Tuple id [showParams c p]
    <> maybe "" (" -> " <>) (retType =<< annotations p) <> ":\n"
    <> showAST (i + 1) a
    where retType [] = Nothing
          retType (Mapping ve ae : _)
            | "return" == showAsName ve = Just $ showExpr ae
          retType (_ : as) = retType as
  Class e _ is a -> (if d then "" else "\n")
    <> indent i "class " <> showExpr (Call e is) <> ":\n" <> showAST (i + 1) a
  s -> indent i $ showStatement' i s

-- | Shows a statement, assuming indentation has already been done.
-- First argument is the indentation level.
showStatement' :: Int -> Statement -> T.Text
showStatement' i = \case
  Pass -> "pass"
  Expr e -> showExpr e
  Return None -> "return"
  Return e -> "return " <> showExpr e
  Raise Nothing -> "raise"
  Raise (Just e) -> "raise " <> showExpr e
  Break -> "break"
  Continue -> "continue"
  Del e -> "del " <> showExpr e
  Import n [] -> "import " <> T.pack n
  Import n [(n1, n2)] | ('.' : n1) `isSuffixOf` n
    -> "import " <> T.pack n <> " as " <> T.pack n2
  Import n ns -> "from " <> m <> " import " <> interMap ", " f ns
    where m          = if null n then "." else T.pack n
          f (n1, n2) = T.pack n1 <> if n1 == n2 then "" else " as " <> T.pack n2
  Assign _ e0 (Binary True o e1 e2) | e0 == e1 ->
    showExpr e0 <> " " <> showBinOp o <> "= " <> showExpr e2
  Assign _ e0 e1 -> showExpr e0 <> " = " <> showExpr e1
  Global es -> "global " <> interMap ", " showExpr es
  If e a -> "if " <> showExpr e <> ":\n" <> showAST (i + 1) a
  Else a -> "else:\n" <> showAST (i + 1) a
  Elif e a -> "elif " <> showExpr e <> ":\n" <> showAST (i + 1) a
  While e a -> "while " <> showExpr e <> ":\n" <> showAST (i + 1) a
  For e0 e1 a -> "for " <> showExpr e0 <> " in " <> showExpr e1 <> ":\n"
    <> showAST (i + 1) a
  Try a -> "try:\n" <> showAST (i + 1) a
  Except Nothing a -> "except:\n" <> showAST (i + 1) a
  Except (Just (e, me)) a -> "except " <> showExpr e
    <> maybe "" (\ e' -> " as " <> showExpr e') me <> ":\n"
    <> showAST (i + 1) a
  Finally a -> "finally:\n" <> showAST (i + 1) a
  With e Nothing a -> "with " <> showExpr e <> ":\n" <> showAST (i + 1) a
  With e0 (Just e1) a -> "with " <> showExpr e0
    <> " as " <> showExpr e1 <> ":\n" <> showAST (i + 1) a
  -- TODO: replace debug output with an error
  s -> tshow s -- error $ "showStatement: Temporary statement " <> show s

-- | Shows an expression.
showExpr :: Expr -> T.Text
showExpr = showExpr' 0

-- | Shows an expression.
-- First argument is the lowest precedence not to enclose in parenthesis.
showExpr' :: Int -> Expr -> T.Text
showExpr' p = \case
  Name n -> T.pack n
  None -> "None"
  Ellipsis -> "..."
  Value _ s -> T.pack s
  Collection Tuple [i] -> inside Tuple id [showItem False "" i <> ","]
  Collection c is -> inside c (showItem False ": ") is
  Slice es -> interMap ":" showNonNone es
  Call e is ->
    par p 17 $ showExpr' 17 e <> inside Tuple id [showArgs is]
  Unary o e -> par p p0 $ showUnOp o <> showExpr' p1 e
    where (p0, p1) = precUnOp o
  Attr e n -> par p 17 $ showExpr' 17 e <> "." <> T.pack n
  Lambda c p' e -> par p 0 $ "lambda " <> showParams c p' <> ": "
    <> showExpr' 0 e
  Comprehension c i cps -> inside c id
    [showItem False ": " i <> T.concat (map showCompPart cps)]
  Conditianal e0 e1 e2 -> par p 1 $ showExpr' 2 e1
    <> " if " <> showExpr' 2 e0 <> " else " <> showExpr' 1 e2
  Binary _ o e1 e2 -> par p p0
    $ showExpr' p1 e1 <> " " <> showBinOp o <> " " <> showExpr' p2 e2
    where (p0, p1, p2) = precBinOp o
  Yield e -> "yield " <> showExpr e
  YieldFrom e -> "yield from " <> showExpr e
  String es -> "f'" <> T.concat (map showAsStringPart es) <> "'"
  Index e i -> par p 17 $ showExpr' 17 e <> inside List showExpr [i]
  -- TODO: replace debug output with an error
  TFunction c _ -> "<code:\n" <> showAST 0 (buildAST c) <> "\n>"
  -- TODO: replace debug output with an error
  e -> tshow e -- error $ "showExpr: Temporary expression " <> show e

-- | Shows parts of a comprehension.
showCompPart :: CompPart -> T.Text
showCompPart = \case
  CPFor e e' -> " for " <> showExpr e <> " in " <> showExpr' 2 e'
  CPIf e -> " if " <> showExpr e

-- | Shows an item.
-- First argument indicates whether to show strings as names.
-- Second argument is a string to use to separate keys and values.
showItem :: Bool -> T.Text -> Item -> T.Text
showItem b s = \case
  Item e -> showExpr e
  ItemUnpack e -> "*" <> showExpr' 18 e
  Mapping e1 e2 | b -> showAsName e1 <> s <> showExpr e2
  Mapping e1 e2 -> showExpr e1 <> s <> showExpr e2
  MappingUnpack e -> "**" <> showExpr' 18 e

-- | Shows the function parameters given a code structure and
-- additional parameter data.
showParams :: Code -> ParamData -> T.Text
showParams Code {..} ParamData {..} = T.concat . intersperse ", "
  $ pos' ++ args ++ kw' ++ kwargs
  where
    -- Keyword argument unpacking.
    kwargs = if flags .&. 0x8 /= 0
      then map ("**" <>) $ take 1 vs''
      else []
    -- Positional argument unpacking.
    (args, vs'') = if
      | flags .&. 0x4 /= 0 -> (map ("*" <>) $ take 1 vs', drop 1 vs')
      | not $ null kw'     -> (["*"], vs')
      | otherwise          -> ([], vs')
    -- Keyword arguments with any defaults.
    kw' = case defaultKW of
      Just is -> map (annotate " = " is) kw
      Nothing -> kw
    -- Keyword arguments.
    (kw, vs') = splitAt kwCount vs
    -- Positional arguments with any defaults.
    pos' = case defaultPos of
      Just is -> take n pos <> zipWith addDefault (drop n pos) is
        where n = length pos - length is
      Nothing -> pos
    -- Positional arguments.
    (pos, vs) = splitAt argCount vars'
    -- Variables, annotated with types.
    vars' = case annotations of
      Just as -> map (annotate ": " as . T.pack) vars
      Nothing -> map T.pack vars
    -- Adds a default value to a parameter.
    addDefault v (Item de) = v <> " = " <> showExpr de
    addDefault _ i         = error $ "addDefualt: Can not handle " ++ show i
    -- Adds an annotation from a mapping to a parameter
    -- First argument is a separator to use.
    annotate _ [] v = v
    annotate s (Mapping ve ae : _) v
      | v == showAsName ve = v <> s <> showExpr ae
    annotate s (_ : as) v = annotate s as v

-- | Shows the function arguments.
showArgs :: [Item] -> T.Text
showArgs is = interMap ", " (showItem True " = ") $ if null complex
  then simple
  else simple ++ [MappingUnpack $ Collection Dict complex]
  where (simple, complex) = partition isSimple is
        isSimple (Mapping k _) = case k of
          Name _      -> True
          Value Str _ -> True
          _           -> False
        isSimple _             = True

-- | Shows an expression unless it is none.
showNonNone :: Expr -> T.Text
showNonNone None = ""
showNonNone e    = showExpr e

-- | Shows the given string in a parenthesis
-- if the first number is larger than the second.
par :: Int -> Int -> T.Text -> T.Text
par p1 p2 t
  | p1 > p2   = "(" <> t <> ")"
  | otherwise = t

-- | Uses the given function to show the items within a specified collection.
inside :: Collection -> (a -> T.Text) -> [a] -> T.Text
inside c f as = l c <> interMap ", " f as <> r c
  where l Tuple = "("
        l List  = "["
        l Set   = "{"
        l Dict  = "{"
        r Tuple = ")"
        r List  = "]"
        r Set   = "}"
        r Dict  = "}"

-- | Shows an expression as if it was a name.
showAsName :: Expr -> T.Text
showAsName (Name n)      = T.pack n
showAsName (Value Str s) = T.pack . init $ tail s
showAsName e             = error
  $ "showAsName: Can not show expression as name " <> show e

-- | Shows a part of a string.
showAsStringPart :: Expr -> T.Text
showAsStringPart (Value Str s) = T.pack . init $ tail s
showAsStringPart (Format e)    = "{" <> showExpr e <> "}"
showAsStringPart e             = error
  $ "showAsStringPart: Can not show expression as string part " <> show e
