{-# LANGUAGE Strict #-}

module Language.Python.Decompile.AST where

import Language.Python.Decompile.Code
import Language.Python.Decompile.Operation

-- | An AST simply consists of a sequence of statements.
type AST = [Statement]

-- | Recursively maps functions over the structure of the AST in top-down order.
recMapAST :: (Expr -> Expr) -> (Statement -> Statement) -> (AST -> AST)
          -> AST -> AST
recMapAST f g h = map (recMapStatement f g h) . h

-- | Represents a Python statement.
data Statement
  -- * Simple statements.
  -- | The no-op statement.
  = Pass
  -- | An expression as a statement.
  | Expr Expr
  -- | A return.
  | Return Expr
  -- | Exception raising.
  | Raise (Maybe Expr)
  -- | Loop break.
  | Break
  -- | Loop continue.
  | Continue
  -- | Deletion.
  | Del Expr
  -- | Variable assignment. The first value indicates a global assignment.
  | Assign Bool Expr Expr
  -- | Mark variables as global.
  | Global [Expr]
  -- | Import statement. First is the module name
  -- , followed by pairs of names to import with a name to import them as.
  | Import Name [(Name, Name)]
  -- * Complex statements.
  -- | Function definition.
  | Def Expr Code ParamData AST
  -- | Decorator.
  | Decorator Expr Statement
  -- | Class definition.
  | Class Expr Code [Item] AST
  -- | If statement.
  | If Expr AST
  -- | Else statement.
  | Else AST
  -- | Else if statement.
  | Elif Expr AST
  -- | While statement.
  | While Expr AST
  -- | For statement. Second expression is the iterator.
  | For Expr Expr AST
  -- | Try statement.
  | Try AST
  -- | Except statement. May specify an exception to catch
  -- and may then specify a name for the exception value.
  | Except (Maybe (Expr, Maybe Expr)) AST
  -- | Finally statement.
  | Finally AST
  -- | With statement. Second expression is the optional variable name.
  | With Expr (Maybe Expr) AST
  -- * Statements introduced for decompilation.
  -- | Collection append used for comprehensions.
  | TAppend Collection Item
  deriving (Show, Eq)

-- | Recursively maps functions over the structure of the statement
-- in top-down order.
recMapStatement :: (Expr -> Expr) -> (Statement -> Statement) -> (AST -> AST)
                -> Statement -> Statement
recMapStatement f g h = rec' . g
  where rec' = \case
          Pass -> Pass
          Expr e -> Expr $ recMapExpr f e
          Return e -> Return $ recMapExpr f e
          Raise me -> Raise $ fmap (recMapExpr f) me
          Break -> Break
          Continue -> Continue
          Del e -> Del $ recMapExpr f e
          Assign b e e' -> Assign b (recMapExpr f e) (recMapExpr f e')
          Global es -> Global $ map (recMapExpr f) es
          Import n nns -> Import n nns
          Def e c p a -> Def (recMapExpr f e) c (recMapParamData f p)
            $ recMapAST f g h a
          Decorator e s -> Decorator (recMapExpr f e) $ recMapStatement f g h s
          Class e c is a -> Class (recMapExpr f e) c (map (recMapItem f) is)
            $ recMapAST f g h a
          If e a -> If (recMapExpr f e) $ recMapAST f g h a
          Else a -> Else $ recMapAST f g h a
          Elif e a -> Elif (recMapExpr f e) $ recMapAST f g h a
          While e a -> While (recMapExpr f e) $ recMapAST f g h a
          For e e' a -> For (recMapExpr f e) (recMapExpr f e')
            $ recMapAST f g h a
          Try a -> Try $ recMapAST f g h a
          Except meme a -> Except (map' <$> meme) $ recMapAST f g h a
            where map' (e, me) = (recMapExpr f e, recMapExpr f <$> me)
          Finally a -> Finally $ recMapAST f g h a
          With e me a -> With (recMapExpr f e) (recMapExpr f <$> me)
            $ recMapAST f g h a
          TAppend c i -> TAppend c $ recMapItem f i

-- | Represents the possible items that appears in Python collections and calls.
data Item
  -- | A simple argument.
  = Item Expr
  -- | Unpacking of a collection of arguments.
  | ItemUnpack Expr
  -- | A simple mapping.
  | Mapping Expr Expr
  -- | Unpacking of a collection of mappings.
  | MappingUnpack Expr
  deriving (Show, Eq)

-- | Recursively maps functions over the structure of the item
-- in top-down order.
recMapItem :: (Expr -> Expr) -> Item -> Item
recMapItem f = \case
  Item e -> Item $ recMapExpr f e
  ItemUnpack e -> ItemUnpack $ recMapExpr f e
  Mapping e e' -> Mapping (recMapExpr f e) (recMapExpr f e')
  MappingUnpack e -> MappingUnpack $ recMapExpr f e

-- | The types of collections in Python.
data Collection = Tuple | List | Set | Dict deriving (Show, Eq)

-- | Represents a Python expression.
data Expr
  -- * Simple values.
  -- | Global or local variable name.
  = Name Name
  -- | The None value.
  | None
  -- | Ellipsis.
  | Ellipsis
  -- | Basic values.
  | Value BasicType String
  -- * Complex expressions.
  -- | Generalized expression for tuples, lists, sets and maps.
  | Collection Collection [Item]
  -- | Slices.
  | Slice [Expr]
  -- | Lambda expression.
  | Lambda Code ParamData Expr
  -- | Comprehension.
  | Comprehension Collection Item [CompPart]
  -- | If expression. First expression is the condition, the other expressions
  -- are the true and false cases in that order.
  | Conditianal Expr Expr Expr
  -- | Unary operation.
  | Unary UnOp Expr
  -- | Binary operation. The first value indicates an in-place operation.
  | Binary Bool BinOp Expr Expr
  -- | Function call.
  | Call Expr [Item]
  -- | Accessing the named attribute from an object.
  | Attr Expr Name
  -- | Indexing.
  | Index Expr Expr
  -- | Yield a value.
  | Yield Expr
  -- | Yield a value from a generator.
  | YieldFrom Expr
  -- | Formatting expression.
  | Format Expr
  -- | String building.
  | String [Expr]
  -- * Expressions introduced for decompliation.
  -- | Function definition as a value.
  | TFunction Code ParamData
  -- | Build class function.
  | TBuildClass
  -- | Used as a placeholder.
  | TDummy
  -- | Used as a placeholder for unpacking.
  | TStar
  -- | Used as a placeholder for tuple assignment.
  | TTuple Int
  -- | A module as a value.
  | TModule Name
  -- | Returned when popping from an empty stack.
  | TEmptyStack
  deriving (Show, Eq)

-- | Recursively maps functions over the structure of the expression
-- in top-down order.
recMapExpr :: (Expr -> Expr) -> Expr -> Expr
recMapExpr f = rec' . f
  where rec' = \case
          Name n -> Name n
          None -> None
          Ellipsis -> Ellipsis
          Value t s -> Value t s
          Collection c is -> Collection c $ map (recMapItem f) is
          Slice es -> Slice $ map (recMapExpr f) es
          Lambda c p e -> Lambda c (recMapParamData f p) $ recMapExpr f e
          Comprehension c i cps -> Comprehension c (recMapItem f i)
            $ map (recMapCompPart f) cps
          Conditianal e e' e'' -> Conditianal (recMapExpr f e)
            (recMapExpr f e') (recMapExpr f e'')
          Unary o e -> Unary o $ recMapExpr f e
          Binary b o e e' -> Binary b o (recMapExpr f e) (recMapExpr f e')
          Call e is -> Call (recMapExpr f e) $ map (recMapItem f) is
          Attr e n -> Attr (recMapExpr f e) n
          Index e e' -> Index (recMapExpr f e) (recMapExpr f e')
          Yield e -> Yield $ recMapExpr f e
          YieldFrom e -> YieldFrom $ recMapExpr f e
          Format e -> Format $ recMapExpr f e
          String es -> String $ map (recMapExpr f) es
          TFunction c d -> TFunction c d
          TBuildClass -> TBuildClass
          TDummy -> TDummy
          TStar -> TStar
          TTuple n -> TTuple n
          TModule n -> TModule n
          TEmptyStack -> TEmptyStack

-- | For and if components of a comprehension.
data CompPart = CPFor Expr Expr | CPIf Expr deriving (Show, Eq)

-- | Recursively maps functions over the structure of the part
-- in top-down order.
recMapCompPart :: (Expr -> Expr) -> CompPart -> CompPart
recMapCompPart f = \case
  CPFor e e' -> CPFor (recMapExpr f e) (recMapExpr f e')
  CPIf e -> CPIf $ recMapExpr f e

-- | Additional information about the function parameters.
data ParamData = ParamData
  { -- | Default values for positional parameters.
    defaultPos  :: Maybe [Item]
    -- | Default values for keyword parameters.
  , defaultKW   :: Maybe [Item]
    -- | Type annotations.
  , annotations :: Maybe [Item]
  } deriving (Show, Eq)

-- | Recursively maps functions over the structure of the parameter data
-- in top-down order.
recMapParamData :: (Expr -> Expr) -> ParamData -> ParamData
recMapParamData f ParamData {..} = ParamData
  { defaultPos  = map (recMapItem f) <$> defaultPos
  , defaultKW   = map (recMapItem f) <$> defaultKW
  , annotations = map (recMapItem f) <$> annotations
  }
