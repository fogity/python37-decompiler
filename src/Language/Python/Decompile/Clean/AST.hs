module Language.Python.Decompile.Clean.AST where

import Language.Python.Decompile.AST
import Language.Python.Decompile.Code
import Language.Python.Decompile.Operation

-- | Performs a series of cleanup steps on the AST.
cleanAST :: AST -> AST
cleanAST
  = recMapAST id id insertPass
  . removeLastReturn True
  . recMapAST id removeRedundantReturn id
  . recMapAST id id cleanClauses
  . recMapAST id id (filter (not . isDirtyStmt))

-- | Returns true if the statement should be removed.
isDirtyStmt :: Statement -> Bool
isDirtyStmt = \case
  Pass -> True
  Expr e -> isDirtyExpr e
  Assign _ (Name n) _ -> n `elem` dirtyNames
  _ -> False

-- | Returns true if the expression should be removed.
isDirtyExpr :: Expr -> Bool
isDirtyExpr = \case
  Attr e _ -> isDirtyExpr e
  TDummy -> True
  TModule _ -> True
  _ -> False

-- | A list of names that usually should be removed.
dirtyNames :: [Name]
dirtyNames = ["__module__", "__qualname__", "__classcell__", "__class__"]

-- | Cleans compound statements.
cleanClauses :: [Statement] -> [Statement]
cleanClauses = \case
  []
    -> []
  If _ [] : Else [] : ss
    -> cleanClauses ss
  If e [] : Else ss1 : ss2
    -> If (Unary Not e) ss1 : cleanClauses ss2
  If e ss0 : Else ss1 : ss2 | Return _ <- last ss0
    -> If e ss0 : cleanClauses (ss1 ++ ss2)
  Else [] : ss
    -> cleanClauses ss
  Else [If e ss0, Else ss1] : ss2
    -> Elif e ss0 : cleanClauses (Else ss1 : ss2)
  Except e ss0 : Finally [] : ss1
    -> Except e ss0 : cleanClauses ss1
  s : ss
    -> s : cleanClauses ss

-- | Removes redundant return statements from function definitions.
removeRedundantReturn :: Statement -> Statement
removeRedundantReturn = \case
  Def e c p a    -> Def e c p $ removeLastReturn False a
  Class e c is a -> Class e c is $ removeLastReturn True a
  s              -> s

-- | Removes the last return of the AST, if it is present.
-- First argument specifies whether to allow removal of a None return.
removeLastReturn :: Bool -> AST -> AST
removeLastReturn _ [] = []
removeLastReturn b a = case last a of
  Return _ | b -> init a
  Return None  -> init a
  If e a'      -> init a ++ [If e $ removeLastReturn b a']
  _            -> a

-- | Inserts a pass statement if the AST is empty.
insertPass :: AST -> AST
insertPass [] = [Pass]
insertPass a  = a
