{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Build.Block where

import Data.Maybe

import Language.Python.Decompile.Block
import Language.Python.Decompile.Build.BasicBlock
import Language.Python.Decompile.Build.Graph
import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

-- | Builds a sequence of nested blocks from a list of instructions.
buildBlocks :: [Instruction] -> BlockSeq
buildBlocks = buildOthers . buildBasics

-- | Builds more complicated blocks from a sequence of basic blocks.
buildOthers :: BlockSeq -> BlockSeq
buildOthers = buildConditionals . buildMost

-- | Builds all blocks except basic blocks and conditionals.
buildMost :: BlockSeq -> BlockSeq
buildMost [] = []
buildMost (b : bs)
  | opcode' b == SETUP_WITH    = buildWith $ b : bs
  | opcode' b == SETUP_EXCEPT  = buildTry BExcept $ b : bs
  | opcode' b == SETUP_FINALLY = buildTry BFinally $ b : bs
  | opcode' b == FOR_ITER      = buildFor $ b : bs
  | otherwise                  = b : buildMost bs

-- | Builds a with block.
buildWith :: BlockSeq -> BlockSeq
buildWith (b : bs) = b : Block'
    { row'     = n
    , block'   = BWith $ buildOthers with
    , opcode'  = NOP
    , target'  = TNext n'
    , hasStmt' = True
    } : buildMost bs1
    where
      Just n           = next' b
      Just m           = jump' b
      (with,      bs0) = span ((< m) . row') bs
      (_   , b' : bs1) = span ((/= END_FINALLY) . opcode') bs0
      Just n'          = next' b'
buildWith [] = error "buildWith: empty list"

-- | Builds a for block.
buildFor :: BlockSeq -> BlockSeq
buildFor (b : bs) = b : Block'
    { row'     = n
    , block'   = BFor $ buildOthers for
    , opcode'  = NOP
    , target'  = TNext m
    , hasStmt' = True
    } : buildMost bs0
    where
      Just n     = next' b
      Just m     = jump' b
      (for, bs0) = span ((< m) . row') bs
buildFor [] = error "buildFor: empty list"

-- | Builds a try block. First argument is the constructor to wrap it in.
buildTry :: (BlockSeq -> BlockSeq -> Block) -> BlockSeq -> BlockSeq
buildTry wrap (b : bs) = b : Block'
    { row'     = n
    , block'   = wrap (buildOthers try) (buildOthers bs1)
    , opcode'  = NOP
    , target'  = t
    , hasStmt' = True
    } : buildMost bs2
    where
      Just n     = next' b
      Just m     = jump' b
      (try, bs0) = span ((< m) . row') bs
      (bs1, bs2) = splitAtMatchingEndFinally 0 bs0
      t          = case bs1 of
        (_ : _) | Just n' <- next' $ last bs1
                  -> TNext n'
        _         -> TNone
buildTry _ [] = error "buildTry: empty list"

-- | Splits the block sequence on the next top-level END_FINALLY.
-- Takes into account nested blocks with finally.
splitAtMatchingEndFinally :: Int -> BlockSeq -> (BlockSeq, BlockSeq)
splitAtMatchingEndFinally _ [] = ([], [])
splitAtMatchingEndFinally 0 (b : bs)
  | opcode' b == END_FINALLY = ([b], bs)
splitAtMatchingEndFinally n (b : bs) = (b : bs1, bs2)
  where
    (bs1, bs2) = splitAtMatchingEndFinally n' bs
    n' = if
      | op == END_FINALLY    -> n - 1
      | op `elem` hasFinally -> n - 1
      | otherwise            -> n
    op = opcode' b
    hasFinally = [SETUP_EXCEPT, SETUP_FINALLY, SETUP_WITH]

-- | Builds conditionals from a sequence of blocks.
buildConditionals :: BlockSeq -> BlockSeq
buildConditionals = concatMap buildIf . partitionBlocks

-- | Partitions a sequence of blocks into independent chunks.
partitionBlocks :: BlockSeq -> [BlockSeq]
partitionBlocks bs = partitionBlocks' fjs bjs bs
  where js  = mapMaybe (\ b -> (row' b,) <$> jump' b) bs
        bjs = filter (uncurry (>)) js
        fjs = filter (uncurry (<)) js

-- | Helper for `partitionBlocks`.
-- First argument is a list of forward jumps.
-- Second argument is a list of backward jumps.
partitionBlocks' :: [(Int, Int)] -> [(Int, Int)] -> BlockSeq -> [BlockSeq]
partitionBlocks' _ _ []  = []
partitionBlocks' _ _ [b] = [[b]]
partitionBlocks' ((_, j) : fjs) bjs (b : bs)
  | row' b > j = partitionBlocks' fjs bjs $ b : bs
partitionBlocks' fjs ((r, _) : bjs) (b : bs)
  | row' b > r = partitionBlocks' fjs bjs $ b : bs
partitionBlocks' fjs @ ((r, _) : _) bjs @ ((_, j) : _) (b : bs)
  | row' b < r, row' b < j = [b] : partitionBlocks' fjs bjs bs
partitionBlocks' fjs bjs (b : bs) = (b : bs') : bss
  where bs' : bss = partitionBlocks' fjs bjs bs

-- | Builds conditionals by building a graph of the program flow.
buildIf :: BlockSeq -> BlockSeq
buildIf [] = []
buildIf bs = buildIfs . mergeLoops . mergePaths . mergeCond
  $ buildGraph (row' $ head bs) [] bs bs

-- | Builds a nested sequence of blocks from a program flow graph.
buildIfs :: Graph -> BlockSeq
buildIfs = \case
  GBlock b         -> [b]
  GCons g g'       -> buildIfs g ++ buildIfs g'
  GBranch b c g g' -> pure Block'
        { row'     = firstRow c [bs, bs']
        , block'   = BIf b c bs bs'
        , opcode'  = NOP
        , target'  = TNone
        , hasStmt' = True
        } where bs  = buildIfs g
                bs' = buildIfs g'
  GLoop c g -> pure Block'
        { row'     = firstRow c [bs]
        , block'   = BWhile c bs
        , opcode'  = NOP
        , target'  = TNone
        , hasStmt' = True
        } where bs = buildIfs g
  _ -> []

-- | Returns the first row it finds.
firstRow :: Cond -> [BlockSeq] -> Int
firstRow c _
  | Just n <- rowC c = n
firstRow _ ((b : _) : _) = row' b
firstRow c (_ : bss)     = firstRow c bss
firstRow _ []            = error "firstRow: No row found."
