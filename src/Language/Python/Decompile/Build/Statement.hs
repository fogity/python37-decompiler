module Language.Python.Decompile.Build.Statement where

import Control.Monad
import Data.Bits
import Safe

import Language.Python.Decompile.AST
import Language.Python.Decompile.Block
import Language.Python.Decompile.Build.Expression
import Language.Python.Decompile.Build.Monad
import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode
import Language.Python.Decompile.Operation

-- | Builds an AST from a Code structure.
buildStatements :: Code -> BlockSeq -> AST
buildStatements c = runBuild c . buildFromBlocks

-- | Builds an AST from a sequence of blocks.
buildFromBlocks :: BlockSeq -> Build AST
buildFromBlocks = fmap concat . mapM buildFromBlock

-- | Builds an AST from a block.
buildFromBlock :: Block' -> Build AST
buildFromBlock b = case block' b of
  BBasic is -> mapM buildStatement is
  BWith bs -> do
    e <- pop
    -- Dummy is used as a placeholder for the as name.
    push TDummy
    with <- buildFromBlocks bs
    return [With e Nothing with]
  BExcept tbs ebs -> do
    try <- buildFromBlocks tbs
    -- Dummies are used as placeholders for the exception values.
    replicateM_ 3 $ push TDummy
    ex <- buildFromBlocks ebs
    return [Try try, Except Nothing ex]
  BFinally tbs fbs -> do
    try <- buildFromBlocks tbs
    fin <- buildFromBlocks fbs
    return [Try try, Finally fin]
  BIf b' c ibs ebs -> do
    pre <- buildFromCond c
    e <- pop
    s <- getStack
    if' <- buildFromBlocks ibs
    e1 <- pop
    putStack s
    when b' $ push e
    else' <- buildFromBlocks ebs
    e2 <- pop
    putStack s
    if b'
      then case lastMay if' of
      Just (Return e1') -> return $ pre ++ [Return $ boolOp e e1']
      _                 -> push (boolOp e e1) >> return pre
      else case (lastMay if', lastMay else') of
      (Just (Return e1'), Just (Return e2'))
        | all (== Pass) $ init if' ++ init else'
        -> return $ pre ++ [Return $ Conditianal e e1' e2']
      _ | all (== Pass) $ if' ++ else'
        -> push (Conditianal e e1 e2) >> return pre
      _ -> return $ pre ++ [If e if', Else else']
    where boolOp (Unary Not ea) eb = Binary False BoolOr  ea eb
          boolOp ea             eb = Binary False BoolAnd ea eb
  BWhile c bs -> do
    pre <- buildFromCond c
    e <- pop
    while <- buildFromBlocks bs
    return $ pre ++ [While e while]
  BFor bs -> do
    e <- pop
    -- Dummy is used as a placeholder for the variable name.
    push TDummy
    for <- buildFromBlocks bs
    return [For TDummy e for]

-- | Builds an AST from a condition.
buildFromCond :: Cond -> Build AST
buildFromCond = \case
  CBlock b -> buildFromBlock b
  CTrue -> push (Value Bool "True") >> return []
  CNot c -> do
    a <- buildFromCond c
    Unary Not <$> pop >>= push
    return a
  CAnd c c' -> andOr c c' BoolAnd
  COr  c c' -> andOr c c' BoolOr
  where andOr c c' o = do
          a  <- buildFromCond c
          e  <- pop
          a' <- buildFromCond c'
          e' <- pop
          push $ Binary False o e e'
          return $ a ++ a'

-- | Builds a statement from an instruction.
-- Non-statement instructions yields a no-op.
buildStatement :: Instruction -> Build Statement
buildStatement Instruction {..} = case opcode of
  -- Break.
  BREAK_LOOP -> return Break
  -- Continue.
  CONTINUE_LOOP -> return Continue
  -- Attribute deletion.
  DELETE_ATTR -> Del <$> (Attr <$> pop <*> getName argument)
  -- Free variable deletion.
  DELETE_DEREF -> Del . Name <$> getCellFree argument
  -- Variable deletion.
  DELETE_FAST -> Del . Name <$> getVar argument
  -- Global variable deletion.
  DELETE_NAME -> Del . Name <$> getName argument
  -- Indexed deletion.
  DELETE_SUBSCR -> Del <$> (flip Index <$> pop <*> pop)
  -- Wildcard import.
  IMPORT_STAR -> pop >>= \case
    TModule m -> return $ Import m [("*", "*")]
    _         -> error "IMPORT_STAR: Expected a module."
  -- List appending used in list comprehensions. The list remains on the stack.
  LIST_APPEND -> TAppend List . Item <$> pop
  -- Dict appending used in dict comprehensions. The dict remains on the stack.
  MAP_ADD -> TAppend Dict <$> (Mapping <$> pop <*> pop)
  -- Pop the top stack object. We need to add it to the AST.
  POP_TOP -> Expr <$> pop
  -- Raise an exception.
  RAISE_VARARGS -> case argument of
    0 -> return $ Raise Nothing
    1 -> Raise . Just <$> pop
    2 -> Raise . Just <$> (Call <$> pop <*> (pure . Item <$> pop))
    _ -> error $ "RAISE_VARARGS: Unsupported argument " ++ show argument
  -- Return statement.
  RETURN_VALUE -> Return <$> pop
  -- Set appending used in set comprehensions. The set remains on the stack.
  SET_ADD -> TAppend Set . Item <$> pop
  -- Attribute assignment.
  STORE_ATTR -> Assign False <$> (Attr <$> pop <*> getName argument) <*> pop
  -- Free variable assignment.
  STORE_DEREF -> Assign False <$> (Name <$> getCellFree argument) <*> pop
  -- Variable assignment.
  STORE_FAST -> Assign False <$> (Name <$> getVar argument) <*> pop
  -- Global variable assignment.
  STORE_GLOBAL -> Assign True <$> (Name <$> getName argument) <*> pop
  -- Name assignment.
  STORE_NAME -> Assign False <$> (Name <$> getName argument) <*> pop
  -- Index assignment
  STORE_SUBSCR -> Assign False <$> (flip Index <$> pop <*> pop) <*> pop
  -- Advanced tuple pattern matching. Handled in a later stage.
  UNPACK_EX -> Assign False (TTuple $ l + 1 + h) <$> pop >>= \ s
    -> replicateM_ l (push TDummy) >> push TStar
    >> replicateM_ h (push TDummy) >> return s
    where l = argument .&. 0xFF
          h = argument `shiftR` 8
  -- Tuple pattern matching. Handled in a later stage.
  UNPACK_SEQUENCE -> Assign False (TTuple argument) <$> pop >>= \ s
    -> replicateM_ argument (push TDummy) >> return s
  -- Non-statement instructions yield a no-op.
  _ -> buildInStack Instruction {..} >> return Pass
