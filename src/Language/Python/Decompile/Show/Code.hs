module Language.Python.Decompile.Show.Code where

import qualified Data.Text as T

import Language.Python.Decompile.Code
import Language.Python.Decompile.Show

-- | Shows a code structure at the given indentation.
showCode :: Int -> Code -> T.Text
showCode n Code {..}
   = indent n "    flags: " <> tshow flags <> "\n"
  <> indent n " pos args: " <> tshow argCount <> "\n"
  <> indent n "  kw args: " <> tshow kwCount <> "\n"
  <> indent n "    names:\n"
  <> T.concat (zipWith showNumName [0..] names)
  <> indent n "     vars:\n"
  <> T.concat (zipWith showNumName [0..] vars)
  <> indent n "cell vars:\n"
  <> T.concat (zipWith showNumName [0..] cell)
  <> indent n "free vars:\n"
  <> T.concat (zipWith showNumName [length cell..] free)
  <> indent n "     code:\n"
  <> T.concat (map (showInstruction n) insts)
  <> indent n "constants:\n"
  <> interMap "\n" (showValue n) consts
  where
    showNumName :: Int -> Name -> T.Text
    showNumName i s = indent n $ padLeft 4 (tshow i) <> " " <> T.pack s <> "\n"

-- | Shows an instruction at the given indentation.
showInstruction :: Int -> Instruction -> T.Text
showInstruction n Instruction {..} = indent n
  $ row' <> " " <> opcode' <> " " <> argument' <> "\n"
  where row'      = padLeft  3  $ tshow row
        opcode'   = padRight 30 $ tshow opcode
        argument' = padLeft  3  $ tshow argument

-- | Shows a value at the given indentation.
showValue :: Int -> Value -> T.Text
showValue n = \case
  VCode c    -> showCode (n + 1) c
  VNone      -> indent n "None"
  VBasic _ s -> indent n $ T.pack s
  VEllipsis  -> indent n "..."
  VTuple vs  -> indent n $ properTuple (showValue 0) vs
