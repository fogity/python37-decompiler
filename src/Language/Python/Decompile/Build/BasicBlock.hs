{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Build.BasicBlock where

import Data.List
import Data.Maybe

import Language.Python.Decompile.Block
import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

-- | Builds a sequence of basic blocks from a list of instructions.
buildBasics :: [Instruction] -> BlockSeq
buildBasics is = catMaybes $ makeBasic is' : bs
  where ts = extractTargets is
        (is', bs) = buildBasics' ts is

-- | Helper function for 'buildBasics'. First argument is a list of target rows.
-- Also returns any instructions preceding the built blocks.
buildBasics' :: [Int] -> [Instruction] -> ([Instruction], [Maybe Block'])
buildBasics' _ [] = ([], [])
buildBasics' ts (i : is)
  -- Clean out any targets that have been passed.
  | not $ null ts, head ts < row i
  = buildBasics' (tail ts) $ i : is
  -- If this instruction marks the end of a block, build blocks from the
  -- following list of instructions, returning this instruction on it's own.
  | opcode i `elem` blockEnd
  = let (is', bs) = buildBasics' ts is in ([i], makeBasic is' : bs)
  -- If this instruction is a target, build blocks from the following list of
  -- instructions and add this instruction to the start of the first block.
  | not $ null ts, head ts == row i
  = let (is', bs) = buildBasics' (tail ts) is in ([], makeBasic (i : is') : bs)
  -- Otherwise, build blocks form the following instructions and include this
  -- instruction with the instructions preceding the blocks.
  | otherwise
  = let (is', bs) = buildBasics' ts is in (i : is', bs)

-- | Builds a basic block from a list of instructions.
-- Returns 'Nothing' if the list of instructions is empty.
makeBasic :: [Instruction] -> Maybe Block'
makeBasic [] = Nothing
makeBasic is = Just Block'
  { row'    = row $ head is
  , block'  = BBasic is
  , opcode' = opcode l
  , target' = if
      | opcode l `elem` branches -> TBranch (row l + 2) (argument l)
      | opcode l `elem` jumps    -> TJump $ argument l
      | otherwise                -> TNext $ row l + 2
  , hasStmt' = any ((`elem` makesStatement) . opcode) is
  } where l = last is

-- | Opcodes that result in a statement.
makesStatement :: [OpCode]
makesStatement =
  [ BREAK_LOOP, CONTINUE_LOOP, DELETE_FAST, DELETE_SUBSCR, LIST_APPEND
  , POP_TOP, RAISE_VARARGS, RETURN_VALUE, SET_ADD, STORE_ATTR
  , STORE_DEREF, STORE_FAST, STORE_GLOBAL, STORE_NAME, STORE_SUBSCR
  , UNPACK_EX, UNPACK_SEQUENCE
  ]

-- | Extracts all jump targets from a list of instructions.
extractTargets :: [Instruction] -> [Int]
extractTargets = sort . map argument . filter (flip elem hasTarget . opcode)

-- | Opcodes that pops from the stack conditionally.
condPop :: [OpCode]
condPop = [JUMP_IF_FALSE_OR_POP, JUMP_IF_TRUE_OR_POP]

-- | Opcodes that turn into if statements.
ifs :: [OpCode]
ifs = condPop ++ [POP_JUMP_IF_FALSE, POP_JUMP_IF_TRUE]

-- | Opcodes that branches the program flow.
branches :: [OpCode]
branches = ifs
  ++ [FOR_ITER, SETUP_EXCEPT, SETUP_FINALLY, SETUP_LOOP, SETUP_WITH]

-- | Opcodes that always jumps.
jumps :: [OpCode]
jumps = [JUMP_ABSOLUTE, JUMP_FORWARD]

-- | Opcodes that jumps or branches.
hasTarget :: [OpCode]
hasTarget = branches ++ jumps

-- | Opcodes that mark the end of a block.
blockEnd :: [OpCode]
blockEnd = hasTarget ++ [END_FINALLY, RETURN_VALUE, WITH_CLEANUP_FINISH]
