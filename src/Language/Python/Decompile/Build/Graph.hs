module Language.Python.Decompile.Build.Graph where

import Control.Applicative

import Language.Python.Decompile.Block
import Language.Python.Decompile.Build.BasicBlock

-- | A graph of the program flow in regards to branches and jumps.
data Graph
  -- | Flow returns to the parent.
  = GNone
  -- | Flow returns to a previously visited node.
  | GRevisit Int
  -- | Flows through the block, then returns to the parent.
  | GBlock Block'
  -- | Flows through the first graph, then into the second, then returned.
  | GCons Graph Graph
  -- | Flows to the first graph if the condition is met, otherwise the second.
  -- First argument indicates a conditional pop.
  | GBranch Bool Cond Graph Graph
  -- | Flows through the graph repeatedly until the condition is met.
  | GLoop Cond Graph

-- | Builds a graph from a sequence of blocks.
-- First argument is the first row of the sequence.
-- Second argument is a list of rows of all visited blocks.
-- Third argument is the complete sequence, and fourth is a part of it.
buildGraph :: Int -> [Int] -> BlockSeq -> BlockSeq -> Graph
buildGraph n ns bs0 (b : bs)
  | row' b `elem` ns = GRevisit $ row' b
  | otherwise        = case target' b of
      TBranch _ m | opcode' b `elem` ifs
        -> GBranch (opcode' b `elem` condPop) (CBlock b)
        (buildGraph n ns' bs0 bs)
        $ if m < n then GNone else buildGraph n ns' bs0 bs'
        where (_, bs') = span ((< m) . row') $ if m < row' b then bs0 else bs
      TJump m -> GCons (GBlock b)
        $ if m < n then GNone else buildGraph n ns' bs0 bs'
        where (_, bs') = span ((< m) . row') $ if m < row' b then bs0 else bs
      _     -> GCons (GBlock b) $ buildGraph n ns' bs0 bs
  where ns' = row' b : ns
buildGraph _ _ _ [] = GNone

-- | Merges branches that resulted from and/or expressions.
mergeCond :: Graph -> Graph
mergeCond = \case
  GCons g0 g1 -> GCons (mergeCond g0) (mergeCond g1)
  GBranch b c g0 g1 -> GBranch b c'' g0'' g1''
    where (c' , g0' , g1' ) = mergeBranches c  g0              g1
          (c'', g0'', g1'') = mergeBranches c' (mergeCond g0') (mergeCond g1')
  g -> g

-- | Merges a branch within one of the sub-graphs with the condition
-- if possible.
mergeBranches :: Cond -> Graph -> Graph -> (Cond, Graph, Graph)
mergeBranches c g0 g1
  | GBranch False c' g2 g3 <- g0, not $ hasStmt c', rowG g1 == rowG g2
  = (COr (CNot c) c', g2, g3)
  | GBranch False c' g2 g3 <- g0, not $ hasStmt c', rowG g1 == rowG g3
  = (CAnd c c', g2, g3)
  | GBranch False c' g2 g3 <- g1, not $ hasStmt c', rowG g0 == rowG g2
  = (COr c c', g2, g3)
  | GBranch False c' g2 g3 <- g1, not $ hasStmt c', rowG g0 == rowG g3
  = (CAnd (CNot c) c', g2, g3)
  | otherwise
  = (c, g0, g1)

-- | Indicates whether a condition generates any statements.
hasStmt :: Cond -> Bool
hasStmt = \case
  CBlock b  -> hasStmt' b
  CTrue     -> False
  CNot c    -> hasStmt c
  CAnd c c' -> hasStmt c || hasStmt c'
  COr  c c' -> hasStmt c || hasStmt c'

-- | Special values returned by 'rowG', corresponds to graph leaves.
data SpecialRow = SRNone | SRRevisit Int deriving Eq

-- | Returns the first row of a graph.
rowG :: Graph -> Either SpecialRow Int
rowG GNone        = Left SRNone
rowG (GRevisit n) = Left $ SRRevisit n
rowG (GBlock b)   = Right $ row' b
rowG (GCons g g') = case rowG g of
  Left SRNone -> rowG g'
  r           -> r
rowG (GBranch _ c g g') = case rowC c of
  Nothing -> case rowG g of
    Left SRNone -> rowG g'
    r           -> r
  Just n -> Right n
rowG (GLoop c g) = case rowC c of
  Nothing -> rowG g
  Just n  -> Right n

-- | Returns the first row of a condition.
rowC :: Cond -> Maybe Int
rowC (CBlock b) = Just $ row' b
rowC CTrue      = Nothing
rowC (CNot c)   = rowC c
rowC (CAnd c _) = rowC c
rowC (COr  c _) = rowC c

-- | Merges identical paths in a graph in order to remove code duplication.
mergePaths :: Graph -> Graph
mergePaths = \case
  GCons (GCons g0 g1) g2 -> mergePaths . GCons g0 $ GCons g1 g2
  GCons g0 g1 -> case mergePaths g0 of
    GCons g2 g3 -> mergePaths . GCons g2 $ GCons g3 g1
    g           -> GCons g $ mergePaths g1
  GBranch b c g0 g1 -> GCons (GBranch b c g0' g1') g
    where (g0', g1', g) = mergePaths' (mergePaths g0) (mergePaths g1)
  GLoop c g -> GLoop c $ mergePaths g
  g -> g

-- | Helper for 'mergePaths' that takes two graphs and extracts a common tail.
mergePaths' :: Graph -> Graph -> (Graph, Graph, Graph)
mergePaths' g0 g1 = case (rowG g0, rowG g1) of
  (r0, r1) | r0 == r1 -> (GNone, GNone, g0)
  (Left (SRRevisit _), _)
    | GCons g2 g3 <- g1, (g0', g3', g) <- mergePaths' g0 g3
      -> (g0', GCons g2 g3', g)
  (_, Left (SRRevisit _))
    | GCons g2 g3 <- g0, (g3', g1', g) <- mergePaths' g3 g1
      -> (GCons g2 g3', g1', g)
  (Right n0, Right n1)
    | n0 > n1, GCons g2 g3 <- g1, (g0', g3', g) <- mergePaths' g0 g3
      -> (g0', GCons g2 g3', g)
    | n0 < n1, GCons g2 g3 <- g0, (g3', g1', g) <- mergePaths' g3 g1
      -> (GCons g2 g3', g1', g)
  _ -> (g0, g1, GNone)

-- | Merges looping graphs into loops.
mergeLoops :: Graph -> Graph
mergeLoops = snd . mergeLoops'

-- | Helper for 'mergeLoops'.
mergeLoops' :: Graph -> (Maybe Int, Graph)
mergeLoops' = \case
  GRevisit n -> (Just n, GNone)
  GCons g0 g1
    | Just _ <- r0 -> (r0, g0')
    | Just n <- r1 -> if rowG g0 == Right n
      then (Nothing, GLoop CTrue $ GCons g0' g1')
      else (r1, GCons g0' g1')
    | otherwise -> (Nothing, GCons g0' g1')
    where (r0, g0') = mergeLoops' g0
          (r1, g1') = mergeLoops' g1
  GBranch b c g0 g1 -> case (r0, r1) of
    (Just n, Just m) | n == m -> if rowC c == Just n
      then (Nothing, GLoop CTrue $ GBranch b c g0' g1')
      else (r0, GBranch b c g0' g1')
    (Just n, _) | rowC c == Just n
      -> (r1, GCons (GLoop c g0') g1')
    (_, Just m) | rowC c == Just m
      -> (r0, GCons (GLoop (CNot c) g1') g0')
    (Just _, Just _) -> error "mergeLoops': Conflicting loops."
    _ -> (r0 <|> r1, GBranch b c g0' g1')
    where (r0, g0') = mergeLoops' g0
          (r1, g1') = mergeLoops' g1
  g -> (Nothing, g)
