{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Operation where

-- | Python unary operations.
data UnOp = Invert | Negative | Not | Positive deriving (Show, Eq)

-- | Python binary operations.
data BinOp
  = Less
  | LessEqual
  | Equal
  | NotEqual
  | Greater
  | GreaterEqual
  | In
  | NotIn
  | Is
  | IsNot
  | ExceptionMatch
  | BAD
  | Add
  | And
  | FloorDivide
  | LeftShift
  | MatrixMultiply
  | Modulo
  | Multiply
  | Or
  | Power
  | RightShift
  | Subtract
  | Divide
  | XOr
  | BoolAnd
  | BoolOr
  deriving (Show, Eq)

-- | Maps a byte to a comparison operation.
toCompareOp :: Int -> BinOp
toCompareOp = \case
  0 -> Less
  1 -> LessEqual
  2 -> Equal
  3 -> NotEqual
  4 -> Greater
  5 -> GreaterEqual
  6 -> In
  7 -> NotIn
  8 -> Is
  9 -> IsNot
  10 -> ExceptionMatch
  11 -> BAD
  n -> error $ "Invalid Python 3.7 compare op: " ++ show n
