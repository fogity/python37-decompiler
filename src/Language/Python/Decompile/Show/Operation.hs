{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Show.Operation where

import qualified Data.Text as T

import Language.Python.Decompile.Operation

-- | Shows a unary operator.
showUnOp :: UnOp -> T.Text
showUnOp = \case
  Invert -> "~"
  Negative -> "-"
  Not -> "not "
  Positive -> "+"

-- | Returns the precedences for the operator.
-- First one is that of the operator itself, the second one is
-- the lowest precedence not to enclose in parenthesis of the operand.
precUnOp :: UnOp -> (Int, Int)
precUnOp = \case
  Invert -> (14, 14)
  Negative -> (14, 14)
  Not -> (4, 4)
  Positive -> (14, 14)

-- | Shows a binary operator.
showBinOp :: BinOp -> T.Text
showBinOp = \case
  Less -> "<"
  LessEqual -> "<="
  Equal -> "=="
  NotEqual -> "!="
  Greater -> ">"
  GreaterEqual -> ">="
  In -> "in"
  NotIn -> "not in"
  Is -> "is"
  IsNot -> "is not"
  ExceptionMatch -> "exception match"
  BAD -> "BAD"
  Add -> "+"
  And -> "&"
  FloorDivide -> "//"
  LeftShift -> "<<"
  MatrixMultiply -> "@"
  Modulo -> "%"
  Multiply -> "*"
  Or -> "|"
  Power -> "**"
  RightShift -> ">>"
  Subtract -> "-"
  Divide -> "/"
  XOr -> "^"
  BoolAnd -> "and"
  BoolOr -> "or"

-- | Returns the precedences for the operator.
-- First one is that of the operator itself, the other ones are
-- the lowest precedences not to enclose in parenthesis for each operand.
precBinOp :: BinOp -> (Int, Int, Int)
precBinOp = \case
  Less -> (5, 6, 6)
  LessEqual -> (5, 6, 6)
  Equal -> (5, 6, 6)
  NotEqual -> (5, 6, 6)
  Greater -> (5, 6, 6)
  GreaterEqual -> (5, 6, 6)
  In -> (5, 6, 6)
  NotIn -> (5, 6, 6)
  Is -> (5, 6, 6)
  IsNot -> (5, 6, 6)
  ExceptionMatch -> (5, 6, 6)
  BAD -> (5, 6, 6)
  Add -> (11, 10, 10)
  And -> (8, 8, 8)
  FloorDivide -> (12, 12, 13)
  LeftShift -> (9, 9, 10)
  MatrixMultiply -> (13, 12, 12)
  Modulo -> (12, 12, 13)
  Multiply -> (13, 12, 12)
  Or -> (6, 6, 6)
  Power -> (15, 16, 15)
  RightShift -> (9, 9, 10)
  Subtract -> (10, 10, 11)
  Divide -> (12, 12, 13)
  XOr -> (7, 7, 7)
  BoolAnd -> (3, 3, 3)
  BoolOr -> (2, 2, 2)
